package ru.vlasova.iteco.taskmanager.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public class FasterXmlSaveCommand extends AbstractCommand {

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_faster_xml_save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save with fasterxml to xml";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IDomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
        domainEndpoint.fasterXmlSave(session);
        terminalService.print("Data has been saved to xml");
    }

}
