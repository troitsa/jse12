package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class TaskSortCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_sort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sorting tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        @Nullable final List<Task> taskList = taskEndpoint.findAllTasksByUserId(session, userId);
        if(taskList == null || taskList.isEmpty()) {
            terminalService.print("There are no tasks. To create: task_create");
            return;
        }
        terminalService.print("Choose sorting option:\n" +
                "1 sorting by creating date\n"+
                "2 sorting by starting date\n"+
                "3 sorting by finish date\n"+
                "4 sorting by ready condition\n"+
                "or leave the field empty");
        @Nullable final String sortMode = terminalService.readString();
        Stream<Task> stream = taskEndpoint.sortTask(taskList, sortMode).stream();
        stream.forEach(e -> terminalService.print(e.getName()));
    }

}
