package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class TaskAttachCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_attach";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Attach task to project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Choose the task and type the number");
        @Nullable final List<Task> taskList = taskEndpoint.findAllTasksByUserId(session, userId);
        if (taskList == null) return;
        if (taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final Task task = taskEndpoint.getTaskByIndex(session, userId, index);
            if(task == null) return;
            terminalService.print("Attach task: " + task.getName() + ". Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = serviceLocator.getProjectEndpoint().getProjectByIndex(session, userId, projectIndex).getId();
            task.setProjectId(projectId);
            terminalService.print("Task attached.");
        }
    }

}