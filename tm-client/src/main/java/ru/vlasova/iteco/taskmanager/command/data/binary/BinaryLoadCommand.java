package ru.vlasova.iteco.taskmanager.command.data.binary;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public class BinaryLoadCommand extends AbstractCommand {

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_binary_load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load binary data";
    }

    @Override
    public void execute() throws Exception {
        TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        @NotNull final IDomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
        domainEndpoint.binaryLoad(session);
        terminalService.print("Data has been loaded. Please, login");

    }

}
