package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.Session;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class StateService {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Getter
    @Setter
    @Nullable
    private Session session;


    @NotNull
    public List<AbstractCommand> getCommands() {
        return  new ArrayList<>(commands.values());
    }

    public void add(@NotNull final AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    @Nullable
    public AbstractCommand get(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return commands.get(command);
    }

}
