package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.Task;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class TaskEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected task";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        System.out.println("Choose the task and type the number");
        @Nullable final List<Task> taskList = taskEndpoint.findAllTasksByUserId(session, userId);
        if (taskList != null && taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final Task task = taskEndpoint.getTaskByIndex(session, userId, index);
            if (task == null) return;
            terminalService.print("Editing task: " + task.getName() + ". Set new name: ");
            @Nullable final String name = terminalService.readString();
            if (name == null) return;
            task.setName(name);
            terminalService.print("Set new description: ");
            @Nullable final String description = terminalService.readString();
            if (description == null) return;
            task.setDescription(description);
            terminalService.print("Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = serviceLocator.getProjectEndpoint()
                    .getProjectByIndex(session, userId, projectIndex).getId();
            if (projectId == null) return;
            task.setProjectId(projectId);
            terminalService.print("Task edit.");
        }
    }

}