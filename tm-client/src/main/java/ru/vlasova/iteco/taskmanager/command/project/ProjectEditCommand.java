package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Project;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "project_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected project";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Choose the task and type the number");
        printProjectList(userId);
        @Nullable final int index = Integer.parseInt(terminalService.readString())-1;
        @Nullable final Project project = projectEndpoint.getProjectByIndex(session, userId, index);
        if(project == null) return;
        terminalService.print("Editing task: " + project.getName() + ". Set new name: ");
        project.setName(terminalService.readString());
        terminalService.print("Task edit.");
    }

}