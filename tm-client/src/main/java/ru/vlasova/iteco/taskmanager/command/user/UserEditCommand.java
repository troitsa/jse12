package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.User;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;


public final class UserEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit user";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Edit user. Set new login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set new password: ");
        @Nullable final String password = terminalService.readString();
        @NotNull final User user = userEndpoint.findUserBySession(session, userId);
        userEndpoint.editUser(session, user, login, password);
        terminalService.print("User saved");
    }

}