package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Session;
import ru.vlasova.iteco.taskmanager.api.endpoint.User;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;


public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_registration";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "To register new user";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.print("Login: ");
        terminalService.print("Creating user. Set login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set password: ");
        @Nullable final String password = terminalService.readString();
        if (login == null || password == null) return;
        @Nullable final User user = userEndpoint.insertUser(login, password);
        if (user == null) {
            terminalService.print("User is not registered. Try again");
        } else {
            Session session = serviceLocator.getSessionEndpoint().createSessionNewUser(login, password);
            userEndpoint.persistUser(user);
            serviceLocator.getStateService().setSession(session);
            terminalService.print("User is registered.");
        }
    }

}