package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Project;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;


public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "project_create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        validSession();
        terminalService.print("Creating project. Set name: ");
        @Nullable final String name = terminalService.readString();
        terminalService.print("Input description: ");
        @Nullable final String description = terminalService.readString();
        terminalService.print("Set start date: ");
        @Nullable final String dateStart = terminalService.readString();
        terminalService.print("Set end date: ");
        @Nullable final String dateFinish = terminalService.readString();
        @Nullable final Project project = projectEndpoint.insertProject(session, userId, name, description, dateStart, dateFinish);
        if(project == null) return;
        projectEndpoint.persistProject(session, project);
        terminalService.print("Project created.");
    }

}