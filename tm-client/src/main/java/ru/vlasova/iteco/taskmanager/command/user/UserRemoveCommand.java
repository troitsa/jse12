package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.User;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.ArrayList;
import java.util.List;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove user";
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final List<User> userList = userEndpoint.findAllUsers(session);
        if(userList == null) {
            terminalService.print("There are no users");
            return;
        }
        terminalService.print("Choose user:");
        for(@NotNull User u : userList) {
            terminalService.print(u.getLogin());
        }
        @Nullable final String userLogin = terminalService.readString();
        userEndpoint.removeUser(session, userEndpoint.checkUser(session, userLogin));
        terminalService.print("User removed");
    }

}
