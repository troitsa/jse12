package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task insertTask(@Nullable final Session session, @Nullable final String userId, @Nullable final String name,
                    @Nullable final String description, @Nullable final String dateStart,
                    @Nullable final String dateFinish) throws Exception;

    @Nullable
    @WebMethod
    Task getTaskByIndex(@Nullable final Session session, @Nullable final String userId, int index) throws Exception;

    @WebMethod
    void removeTasksByProjectId(@Nullable final Session session, @Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    List<Task> getTasksByProjectId(@Nullable final Session session, @Nullable final String userId, @Nullable final String projectId) throws Exception;

    @WebMethod
    void removeTaskByUserId(@Nullable final Session session, @Nullable final String userId, @Nullable final String id) throws Exception;

    @WebMethod
    void removeTaskByIndex(@Nullable final Session session, @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<Task> searchTask(@Nullable final Session session, @Nullable final String userId, @Nullable final String searchString) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasks(@Nullable final Session session) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTasksByUserId(@Nullable final Session session, @Nullable final String userId) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(@Nullable final Session session, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTaskByUserId(@Nullable final Session session, @Nullable final String userId, @Nullable String id) throws Exception;

    @Nullable
    @WebMethod
    Task persistTask(@Nullable final Session session, @Nullable final Task task) throws Exception;

    @WebMethod
    void mergeTask(@Nullable final Session session, @Nullable final Task task) throws Exception;

    @WebMethod
    void removeTask(@Nullable final Session session, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final Session session) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(@Nullable final Session session, @Nullable final String userId) throws Exception;

    @Nullable
    @WebMethod
    List<Task> sortTask(@NotNull final List<Task> taskList, @Nullable final String sortMode) throws Exception;
}
