package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void binarySave(@Nullable final Session session) throws Exception;

    @WebMethod
    void binaryLoad(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterJsonLoad(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterJsonSave(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxBJsonLoad(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxBJsonSave(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlLoad(@Nullable final Session session) throws Exception;

    @WebMethod
    void fasterXmlSave(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxBXmlLoad(@Nullable final Session session) throws Exception;

    @WebMethod
    void jaxBXmlSave(@Nullable final Session session) throws Exception;

    @Nullable
    String getSaveDir();

}
