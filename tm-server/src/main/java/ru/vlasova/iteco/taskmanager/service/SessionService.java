package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.repository.SessionRepository;
import ru.vlasova.iteco.taskmanager.util.DbUtil;
import ru.vlasova.iteco.taskmanager.util.HashUtil;
import ru.vlasova.iteco.taskmanager.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Setter
public final class SessionService implements ISessionService {

    @NotNull
    private IUserService userService;

    @NotNull
    @Getter
    private IPropertyService propertyService;

    public SessionService(@NotNull final IUserService userService,
                          @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        connection.setAutoCommit(false);
        @NotNull ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Session was not removed");
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session create(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findOne(userService.checkUser(login));
        if (user == null) return null;
        password = HashUtil.MD5(password);
        if (password == null) return null;
        if (!password.equals(user.getPwd())) return null;
        Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session,
                propertyService.getSessionSalt(),
                propertyService.getSessionCycle()));
        persist(session);
        return session;
    }

    @Override
    public void validate(@Nullable final Session currentSession) throws Exception {
        final Date currentTime = new Date();
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        if (!contains(currentSession.getId())) throw new AccessDeniedException("Invalid session");
        if (currentSession.getUserId() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getSignature() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getRole() == null) throw new AccessDeniedException("Invalid session");
        @NotNull final Session session = new Session();
        session.setSignature(currentSession.getSignature());
        session.setRole(currentSession.getRole());
        session.setId(currentSession.getId());
        session.setCreateDate(currentSession.getCreateDate());
        session.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature = SignatureUtil.sign
                (session, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        @Nullable final String currentSessionSignature = SignatureUtil.sign
                (currentSession, propertyService.getSessionSalt(), propertyService.getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new AccessDeniedException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new AccessDeniedException("Invalid session");
        if (currentSession.getCreateDate().getTime() - currentTime.getTime() > propertyService.getSessionLifetime())
            throw new AccessDeniedException("Invalid session");
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @NotNull final List<Session> users = repository.findAll();
        connection.close();
        return users;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @Nullable final Session session = repository.findOne(id);
        connection.close();
        return session;
    }

    @Override
    public Session persist(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.persist(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Session was not created");
        } finally {
            connection.close();
        }
        return session;
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.merge(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Session was not updated");
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (session.getSignature() == null) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        connection.setAutoCommit(false);
        @NotNull ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.removeSessionBySignature(session.getSignature());
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Session was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        connection.setAutoCommit(false);
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Sessions was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws Exception {
        if (sessionId == null) return false;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @NotNull final boolean contains = repository.contains(sessionId);
        connection.close();
        return contains;
    }

    @Override
    public void checkSession(@Nullable final Session currentSession, @NotNull final Role role) throws Exception {
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        validate(currentSession);
        if (currentSession.getRole() != role) throw new AccessDeniedException("Access denied");
    }

}
