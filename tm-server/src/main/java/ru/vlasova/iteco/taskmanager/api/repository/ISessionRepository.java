package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @NotNull
    List<Session> findAll() throws SQLException;

    @Nullable
    Session findOne(@NotNull String id) throws SQLException;

    @Nullable
    Session persist(@NotNull Session obj) throws Exception;

    void merge(@NotNull Session obj) throws Exception;

    void remove(@NotNull String id) throws SQLException;

    void removeAll() throws SQLException;

    @Nullable
    Session findByUserId(@NotNull final String userId) throws SQLException;

    void removeSessionBySignature(@NotNull final String signature) throws SQLException;

    boolean contains(@NotNull final String sessionId) throws SQLException;

    @Nullable
    Session fetch(@Nullable final ResultSet row) throws SQLException;

}
