package ru.vlasova.iteco.taskmanager.error;

import org.jetbrains.annotations.NotNull;

public final class DuplicateException extends Exception {

    public DuplicateException(@NotNull final String message) {
        super(message);
    }

}
