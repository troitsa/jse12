package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    Task getTaskByIndex(@NotNull final String userId, int index) throws Exception;

    void removeTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    @Nullable
    List<Task> getTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    @NotNull
    List<Task> search(@NotNull final String userId, @NotNull final String searchString) throws Exception;

    @NotNull
    List<Task> findAll(@NotNull final String userId) throws Exception;

    @Nullable
    Task findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    Task findOne(@NotNull final String id) throws Exception;

    @Nullable
    Task persist(@NotNull final Task obj) throws Exception;

    @NotNull Task merge(@NotNull final Task obj) throws SQLException;

    void remove(@NotNull final String id) throws SQLException;

    void removeAll() throws SQLException;

    Task fetch(@Nullable final ResultSet row) throws Exception;

}
