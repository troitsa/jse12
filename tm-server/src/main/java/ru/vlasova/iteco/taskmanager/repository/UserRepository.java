package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @Nullable
    public String checkUser(@NotNull final String login) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user where login=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @NotNull final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user.getId();
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user where id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @NotNull final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    @Override
    @NotNull
    public User persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "insert into taskmanager.app_user values (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPwd());
        preparedStatement.setString(4, user.getRole().name());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @Override
    public User merge(@NotNull final User user) throws SQLException {
        @NotNull final String query = "update taskmanager.app_user set login=?, passwordHash=?, role=? where id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getPwd());
        preparedStatement.setString(3, user.getRole().name());
        preparedStatement.setString(4, user.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "delete from taskmanager.app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "delete from taskmanager.app_user where id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public User fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPwd((row.getString("passwordHash")));
        user.setRole(Role.valueOf(row.getString("role")));
        return user;
    }

}