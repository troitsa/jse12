package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    Project insertProject(@Nullable Session session, @Nullable final String userId,
                          @Nullable final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjects(@Nullable final Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjectsByUserId(@Nullable final Session session, @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(@Nullable final Session session, @NotNull String id) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProjectByUserId(@Nullable final Session session, @NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    @WebMethod
    Project persistProject(@Nullable final Session session, @NotNull Project project) throws Exception;

    @WebMethod
    void mergeProject(@Nullable final Session session, @NotNull Project project) throws Exception;

    @WebMethod
    void removeProjectById(@Nullable final Session session, @NotNull String id) throws Exception;

    @WebMethod
    void removeProjectByUserId(@Nullable final Session session, @NotNull String userId, @NotNull String id) throws Exception;

    @WebMethod
    void removeAllProjects(@Nullable final Session session) throws Exception;

    @WebMethod
    void removeAllProjectByUserId(@Nullable final Session session, @NotNull String userId) throws Exception;

    @Nullable
    @WebMethod
    Project getProjectByIndex(@Nullable final Session session, @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<Task> getTasksByProjectIndex(@Nullable final Session session, @Nullable final String userId, int projectIndex) throws Exception;

    @Nullable
    @WebMethod
    List<Project> searchProject(@Nullable final Session session, @Nullable final String userId, @Nullable final String searchString) throws Exception;

    @Nullable
    @WebMethod
    List<Project> sortProject(@NotNull final List<Project> projectList, @Nullable final String sortMode) throws Exception;

}
