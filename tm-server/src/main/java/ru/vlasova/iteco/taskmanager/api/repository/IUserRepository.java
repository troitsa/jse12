package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Nullable
    String checkUser(@NotNull final String login) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(@NotNull final String id) throws Exception;

    @Nullable
    User persist(@NotNull final User obj) throws DuplicateException, SQLException;

    User merge(@NotNull final User obj) throws SQLException;

    void remove(@NotNull final String id) throws SQLException;

    void removeAll() throws SQLException;

    User fetch(@Nullable final ResultSet row) throws Exception;

}
