package ru.vlasova.iteco.taskmanager.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;
import ru.vlasova.iteco.taskmanager.util.DbUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@NoArgsConstructor
public final class TaskService implements ITaskService {

    @Override
    public void removeTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        if (userId == null || projectId == null) return;
        repository.removeTasksByProjectId(userId, projectId);
        connection.close();
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        if (userId == null || projectId == null) return null;
        @NotNull final List<Task> taskList = repository.getTasksByProjectId(userId, projectId);
        connection.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Task task = new Task(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        if (userId == null) return;
        @Nullable final Task task = getTaskByIndex(userId, index);
        if (task == null) return;
        repository.remove(userId, task.getId());
        connection.close();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.remove(userId,id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Task was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        if (userId == null || index < 0 ) return null;
        @Nullable final Task task = repository.getTaskByIndex(userId, index);
        connection.close();
        return task;
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) throws Exception {
        if (userId == null || searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final List<Task> taskList = repository.search(userId, searchString);
        connection.close();
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) throws Exception {
        if (userId == null) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final List<Task> taskList = repository.findAll(userId);
        connection.close();
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.merge(entity);
        connection.close();
    }

    @Override
    @Nullable
    public Task persist(@Nullable final Task task) throws Exception {
        if (task == null) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.persist(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Task was not created");
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        @NotNull final List<Task> taskList = repository.findAll();
        connection.close();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findOne(id);
        connection.close();
        return task;
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findOneByUserId(userId,id);
        connection.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Task was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Tasks was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Tasks was not removed");
        } finally {
            connection.close();
        }
    }

}
