package ru.vlasova.iteco.taskmanager.comparator;

import ru.vlasova.iteco.taskmanager.entity.Item;

import java.util.Comparator;

public class ByNameComparator<T extends Item> implements Comparator<T> {

    @Override
    public int compare(final T o1, final T o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
