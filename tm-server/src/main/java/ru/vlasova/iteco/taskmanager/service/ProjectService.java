package ru.vlasova.iteco.taskmanager.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;
import ru.vlasova.iteco.taskmanager.util.DbUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Setter
public final class ProjectService implements IProjectService {

    @NotNull
    private ITaskService taskService;

    public ProjectService(@NotNull final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    @Nullable
    public Project insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final String dateStart,
                          @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Project project = new Project(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        if (userId == null) return;
        @Nullable final Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        repository.remove(userId, project.getId());
        connection.close();
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        @Nullable Project project;
        try {
            project = repository.findOne(id);
            repository.remove(userId,id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Project was not removed");
        } finally {
            connection.close();
        }
        if (project != null) taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    @Nullable
    public Project getProjectByIndex(@Nullable final String userId, final int index) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        if (userId == null || index < 0 ) return null;
        @Nullable final String projectId = repository.getIdByIndex(userId, index);
        if (projectId == null) return null;
        @Nullable final Project project = repository.findOne(projectId);
        connection.close();
        return project;
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectIndex(@Nullable final String userId, final int projectIndex) throws Exception {
        if (userId == null || projectIndex < 0) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final String projectId = repository.getIdByIndex(userId, projectIndex);
        connection.close();
        if (projectId == null) return null;
        return taskService.getTasksByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public List<Project> search(@Nullable final String userId, @Nullable final String searchString) throws Exception {
        if (userId == null || searchString == null || searchString.trim().isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<Project> projectList = repository.search(userId, searchString);
        connection.close();
        return projectList;
    }

    @Override
    @Nullable
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<Project> projectList = repository.findAll(userId);
        connection.close();
        return projectList;
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.merge(entity);
        connection.close();
    }

    @Override
    @Nullable
    public Project persist(@Nullable final Project project) throws Exception {
        if (project == null) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.persist(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Project was not created");
        } finally {
            connection.close();
        }
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        @NotNull final List<Project> projectList = repository.findAll();
        connection.close();
        return projectList;
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findOne(id);
        connection.close();
        return project;
    }

    @Override
    @Nullable
    public Project findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findOneByUserId(userId,id);
        connection.close();
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Project was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Projects was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Projects was not removed");
        } finally {
            connection.close();
        }

    }

}
