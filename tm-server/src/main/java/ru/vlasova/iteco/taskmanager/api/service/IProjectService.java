package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.SQLException;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project insert(@Nullable final String userId, @Nullable final String name,
                   @Nullable final String description, @Nullable final String dateStart,
                   @Nullable final String dateFinish);

    @Nullable
    Project getProjectByIndex(@Nullable final String userId, int index) throws Exception;

    void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void remove(@Nullable final String userId, int index) throws Exception;

    @Nullable
    List<Task> getTasksByProjectIndex(@Nullable final String userId, int projectIndex) throws Exception;

    @Nullable
    List<Project> search(@Nullable final String userId, @Nullable final String searchString) throws Exception;

    @Nullable
    List<Project> findAll() throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    Project findOne(@Nullable final String id) throws Exception;

    @Nullable
    Project findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    Project persist(@Nullable final Project obj) throws DuplicateException, SQLException, Exception;

    void merge(@Nullable final Project obj) throws Exception;

    void remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

}
