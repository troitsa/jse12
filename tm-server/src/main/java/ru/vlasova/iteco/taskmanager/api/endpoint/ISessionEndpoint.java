package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    void removeSession(@Nullable final Session session) throws Exception;

    @Nullable
    @WebMethod
    Session findOneSession(@Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    Session createSessionNewUser(@NotNull final String login, @NotNull final String password) throws Exception;

}
