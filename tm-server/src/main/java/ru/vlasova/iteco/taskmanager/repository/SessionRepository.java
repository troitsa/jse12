package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ISessionRepository;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull List<Session> findAll() throws SQLException {
        @NotNull final String query = "select * from app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next()) sessions.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "select * from app_session where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "delete from app_session where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public Session persist(@NotNull final Session session) throws Exception {
        @NotNull final String query = "insert into app_session values (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getRole().name());
        statement.setString(4, session.getSignature());
        statement.setDate(5, DateUtil.parseDateToSQLDate(session.getCreateDate()));
        statement.executeUpdate();
        connection.commit();
        statement.close();
        return session;
    }

    @Override
    public void merge(@NotNull final Session session) throws Exception {
        @NotNull final String query =
                "update app_session set userId = ?, signature = ?, creationTime = ?, role = ? where id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getUserId());
        statement.setString(2, session.getSignature());
        statement.setDate(3, DateUtil.parseDateToSQLDate(session.getCreateDate()));
        statement.setString(4, session.getRole().name());
        statement.setString(5, session.getId());
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "delete from app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "select * from app_session where userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature) throws SQLException {
        @NotNull final String query = "delete from app_session where signature = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1,signature);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public boolean contains(@NotNull final String sessionId) throws SQLException {
        @NotNull final String query = "select exists (select id from app_session where id = ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        @NotNull boolean result = resultSet.getInt(1) >= 1;
        statement.close();
        return result;
    }

    @Override
    @Nullable
    public Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.valueOf(row.getString("role")));
        session.setSignature(row.getString("signature"));
        session.setCreateDate(row.getDate("createDate"));
        return session;
    }

}
