package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Status;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private final Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@NotNull final String userId, int index) throws Exception {
        @Nullable final List<Task> taskList = findAll(userId);
        @Nullable final Task task = taskList.get(index);
        if (task == null) return null;
        return task;
    }

    @Override
    public void removeTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement
                ("delete from taskmanager.app_task where user_id= ? & project_id= ?");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
    }

    @Override
    @NotNull
    public List<Task> getTasksByProjectId(@NotNull final String userId,
                                          @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @Nullable final List<Task> tasks = findAll(userId);
        for (@NotNull Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> search(@NotNull final String userId, @NotNull final String searchString) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where user_id = ? and (name = ? or description = ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, searchString);
        preparedStatement.setString(3, searchString);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where user_id=? and id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement
                ("delete from taskmanager.app_task where user_id=? and id=?");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement
                ("delete from taskmanager.app_task where user_id = ?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Override
    @NotNull
    public Task persist(@NotNull final Task task) throws Exception {
        @NotNull final String query = "insert into taskmanager.app_task values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getUserId());
        preparedStatement.setString(3, task.getName());
        preparedStatement.setString(4, task.getDescription());
        preparedStatement.setDate(5, new java.sql.Date(task.getDateCreate().getTime()));
        preparedStatement.setDate(6, DateUtil.parseDateToSQLDate(task.getDateStart()));
        preparedStatement.setDate(7, DateUtil.parseDateToSQLDate(task.getDateFinish()));
        preparedStatement.setString(8, task.getStatus().name());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @Override
    public @NotNull Task merge(@NotNull final Task task) throws SQLException {
        @NotNull final String query = "update taskmanager.app_task set user_id=?," +
                " dateCreate = ?, name = ?, description = ?, status = ?, dateStart = ?, dateFinish = ? where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getUserId());
        preparedStatement.setDate(2, new java.sql.Date(task.getDateCreate().getTime()));
        preparedStatement.setString(3, task.getName());
        preparedStatement.setString(4, task.getDescription());
        preparedStatement.setString(5, String.valueOf(task.getStatus()));
        preparedStatement.setDate(6, DateUtil.parseDateToSQLDate(task.getDateStart()));
        preparedStatement.setDate(7, DateUtil.parseDateToSQLDate(task.getDateFinish()));
        preparedStatement.setString(8, task.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_task");
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_task where id = ?");
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public Task fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setDateCreate(row.getDate("dateCreate"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setDateStart(row.getDate("dateStart"));
        task.setDateFinish(row.getDate("dateFinish"));
        return task;
    }

}
