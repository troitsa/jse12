package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Status;

import java.util.Date;
import java.util.UUID;

public class Item extends AbstractEntity {

    @Setter
    @Getter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Setter
    @Getter
    @Nullable
    protected String userId;

    @Setter
    @Getter
    @Nullable
    protected String name = "";

    @Setter
    @Getter
    @Nullable
    protected String description = "";

    @Setter
    @Getter
    @NotNull
    protected Date dateCreate = new Date(System.currentTimeMillis());

    @Setter
    @Getter
    @Nullable
    protected Date dateStart;

    @Setter
    @Getter
    @Nullable
    protected Date dateFinish;

    @Setter
    @Getter
    @NotNull
    protected Status status = Status.PLANNED;

}
