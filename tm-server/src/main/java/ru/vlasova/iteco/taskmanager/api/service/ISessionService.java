package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void remove(@Nullable final String id) throws Exception;

    @NotNull
    Session create(@Nullable final String login, @Nullable final String pass) throws Exception;

    void validate(@Nullable final Session session) throws Exception;

    @Nullable
    List<Session> findAll() throws Exception;

    @Nullable
    Session findOne(@Nullable final String id) throws Exception;

    @Nullable
    Session persist(@Nullable final Session obj) throws Exception;

    void merge(@Nullable final Session obj) throws Exception;

    void remove(@Nullable final Session session) throws Exception;

    void removeAll() throws Exception;

    boolean contains(@Nullable final String sessionId) throws Exception;

    void checkSession(@Nullable final Session currentSession, @NotNull final Role role) throws Exception;
}
