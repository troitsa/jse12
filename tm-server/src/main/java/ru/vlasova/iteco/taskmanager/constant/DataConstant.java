package ru.vlasova.iteco.taskmanager.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class DataConstant {

    @NotNull public static final String DIR_SAVE = System.getProperty("user.dir") + File.separator + "dataload" + File.separator;

    @NotNull public static final String DIR_RESOURCE = System.getProperty("user.dir") + File.separator +
            "tm-server"  + File.separator + "src"  + File.separator + "main"
            + File.separator + "resources" + File.separator;
}
