package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User insert(@Nullable final String login, @Nullable final String password);

    @Nullable
    User doLogin(@Nullable final String login, @Nullable final String password) throws Exception;

    @Nullable
    String checkUser(@Nullable final String login) throws Exception;

    void edit(@Nullable final User user, @Nullable final String login, @Nullable final String password) throws Exception;

    boolean checkRole(@Nullable final String userId, @Nullable final List<Role> roles) throws Exception;

    @Nullable
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(@Nullable final String id) throws Exception;

    @Nullable
    User persist(@Nullable final User obj) throws Exception;

    void merge(@Nullable final User obj) throws Exception;

    void remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

}