package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.entity.Domain;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

public interface IDomainService {

    void save(@NotNull final Domain domain) throws Exception;
    void load(@NotNull final Domain domain) throws Exception;

}
