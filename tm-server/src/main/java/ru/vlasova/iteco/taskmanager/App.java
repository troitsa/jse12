package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.context.Bootstrap;

public final class App {

    public static void main(@Nullable final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}