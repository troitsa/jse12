package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task insert(@Nullable final String userId, @Nullable final String name,
                @Nullable final String description, @Nullable final String dateStart,
                @Nullable final String dateFinish);

    @Nullable
    Task getTaskByIndex(@Nullable final String userId, int index) throws Exception;

    void removeTasksByProjectId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    List<Task> getTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    void remove(@Nullable final String userId, int index) throws Exception;

    @Nullable
    List<Task> search(@Nullable final String userId, @Nullable final String searchString) throws Exception;

    @Nullable
    List<Task> findAll() throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    Task findOne(@Nullable final String id) throws Exception;

    @Nullable
    Task findOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    Task persist(@Nullable final Task obj) throws DuplicateException, Exception;

    void merge(@Nullable final Task obj) throws Exception;

    void remove(@Nullable final String id) throws Exception;

    void removeAll() throws Exception;

    void removeAll(@Nullable final String userId) throws Exception;

}
