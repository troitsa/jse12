package ru.vlasova.iteco.taskmanager.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.util.DbUtil;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@NoArgsConstructor
public final class UserService implements IUserService {

    @Override
    public void edit(@Nullable final User user, @Nullable final String login, @Nullable final String password) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        if (!isValid(login, password) || user == null) return;
        user.setLogin(login);
        user.setPwd(HashUtil.MD5(password));
        connection.setAutoCommit(false);
        try {
            repository.merge(user);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("User was not updated");
        } finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    public User insert(@Nullable final String login, @Nullable final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    @Nullable
    public User doLogin(final String login, final String password) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final String id = checkUser(login);
        if (id == null) return null;
        @Nullable final User user = repository.findOne(id);
        connection.close();
        if (user == null) return null;
        @Nullable final String pwd = user.getPwd();
        if (pwd == null) return null;
        if (pwd.equals(HashUtil.MD5(password))) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @Nullable
    public String checkUser(final String login) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        if (!isValid(login)) return null;
        @Nullable String id = repository.checkUser(login);
        connection.close();
        if (id == null) return null;
        return id;
    }

    @Override
    public boolean checkRole(@Nullable final String userId, @Nullable final List<Role> roles) throws Exception {
        if (userId == null || roles == null) return false;
        @Nullable User user = findOne(userId);
        if (user == null) return false;
        for (@Nullable Role role : roles) {
            if (user.getRole().equals(role)) return true;
        }
        return false;
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        if (entity == null) return;
        repository.merge(entity);
        connection.close();
    }

    @Override
    @Nullable
    public User persist(@Nullable final User entity) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        if (entity == null) return null;
        repository.persist(entity);
        connection.close();
        return entity;
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @NotNull final List<User> users = repository.findAll();
        connection.close();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        if (id == null) return null;
        @Nullable final User user = repository.findOne(id);
        connection.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("User was not removed");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = DbUtil.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("User was not removed");
        } finally {
            connection.close();
        }
    }

}
