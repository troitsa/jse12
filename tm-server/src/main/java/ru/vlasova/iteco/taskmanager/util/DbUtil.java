package ru.vlasova.iteco.taskmanager.util;

import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import static ru.vlasova.iteco.taskmanager.constant.DataConstant.DIR_RESOURCE;

public final class DbUtil {
    @NotNull
    public static Connection getConnection() throws  Exception {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream propFile = new FileInputStream( DIR_RESOURCE + "db.properties");
        properties.load(propFile);
        Class.forName(properties.getProperty("jdbcDriver"));
        return DriverManager.getConnection(properties.getProperty("db.host"), properties.getProperty("db.login"),
                properties.getProperty("db.password"));
    }

}
