package ru.vlasova.iteco.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    @Nullable
    public static Date parseDateFromString(@Nullable String dateString){
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("d.MM.yyyy");
        Date result;
        try {
            result = format.parse(dateString);
        } catch (ParseException e){
            return null;
        }
        return result;
    }

    @NotNull private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d.MM.yyyy");

    @NotNull
    public static String parseDateToString(@Nullable final Date date){
        return date != null ? dateFormat.format(date) : "undefined";
    }

    public static java.sql.Date parseDateToSQLDate(@Nullable final Date date) {
        if(date == null) return null;
        return new java.sql.Date(date.getTime());
    }

}