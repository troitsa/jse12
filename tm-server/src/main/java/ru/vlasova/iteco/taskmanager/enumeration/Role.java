package ru.vlasova.iteco.taskmanager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String roleName;

    Role(@NotNull String roleName) {
        this.roleName = roleName;
    }

    @NotNull
    public String displayName() {
        return roleName;
    }

}
