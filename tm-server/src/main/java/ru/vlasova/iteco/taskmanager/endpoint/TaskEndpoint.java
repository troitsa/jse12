package ru.vlasova.iteco.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.comparator.*;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.Task;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private ITaskService taskService;

    public TaskEndpoint() {
        super();
    }

    public TaskEndpoint(@NotNull final ISessionService sessionService,
                        @NotNull final ITaskService taskService) {
        super(sessionService);
        this.taskService = taskService;
    }

    @Override
    @Nullable
    public Task insertTask(@Nullable final Session session,
                           @Nullable final String userId,
                           @Nullable final String name,
                           @Nullable final String description,
                           @Nullable final String dateStart,
                           @Nullable final String dateFinish) throws Exception {
        validateSession(session);
        return taskService.insert(userId, name, description, dateStart, dateFinish);
    }

    @Override
    public @Nullable Task getTaskByIndex(@Nullable final Session session,
                                         @Nullable final String userId,
                                         int index) throws Exception {
        validateSession(session);
        return taskService.getTaskByIndex(userId, index);
    }

    @Override
    public void removeTasksByProjectId(@Nullable final Session session,
                                       @Nullable final String userId,
                                       @Nullable final String id) throws Exception {
        validateSession(session);
        taskService.removeTasksByProjectId(userId, id);
    }

    @Override
    public @Nullable List<Task> getTasksByProjectId(@Nullable final Session session,
                                                    @Nullable final String userId,
                                                    @Nullable final String projectId) throws Exception {
        validateSession(session);
        return taskService.getTasksByProjectId(userId, projectId);
    }

    @Override
    public void removeTaskByUserId(@Nullable final Session session,
                           @Nullable final String userId,
                           @Nullable final String id) throws Exception {
        validateSession(session);
        taskService.remove(userId, id);
    }

    @Override
    public void removeTaskByIndex(@Nullable final Session session,
                                  @Nullable final String userId,
                                  int index) throws Exception {
        validateSession(session);
        taskService.remove(userId,index);
    }

    @Override
    public @Nullable List<Task> searchTask(@Nullable final Session session,
                                           @Nullable final String userId,
                                           @Nullable final String searchString) throws Exception {
        validateSession(session);
        return taskService.search(userId, searchString);
    }

    @Override
    public @Nullable List<Task> findAllTasks(@Nullable final Session session) throws Exception {
        validateSession(session);
        return taskService.findAll();
    }

    @Override
    public @Nullable List<Task> findAllTasksByUserId(@Nullable final Session session,
                                                     @Nullable final String userId) throws Exception {
        validateSession(session);
        return taskService.findAll(userId);
    }

    @Override
    public @Nullable Task findOneTask(@Nullable final Session session,
                                      @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.findOne(id);
    }

    @Override
    public @Nullable Task findOneTaskByUserId(@Nullable final Session session,
                                              @Nullable final String userId,
                                              @Nullable final String id) throws Exception {
        validateSession(session);
        return taskService.findOneByUserId(userId, id);
    }

    @Override
    public @Nullable Task persistTask(@Nullable final Session session,
                                      @Nullable final Task task) throws Exception {
        validateSession(session);
        return taskService.persist(task);
    }

    @Override
    public void mergeTask(@Nullable final Session session,
                          @Nullable final Task task) throws Exception {
        validateSession(session);
        taskService.merge(task);
    }

    @Override
    public void removeTask(@Nullable final Session session,
                           @Nullable final String id) throws Exception {
        validateSession(session);
        taskService.remove(id);
    }

    @Override
    public void removeAllTasks(@Nullable final Session session) throws Exception {
        validateSession(session);
        taskService.removeAll();
    }

    @Override
    public void removeAllTasksByUserId(@Nullable final Session session,
                                       @Nullable final String userId) throws Exception {
        validateSession(session);
        taskService.removeAll(userId);
    }

    @Override
    @NotNull
    public List<Task> sortTask(@NotNull final List<Task> taskList, @Nullable final String sortMode) throws Exception {
        @Nullable Comparator<Task> comparator = new ByNameComparator<>();
        if(sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    comparator = new DateCreateComparator<>();
                    break;
                case ("2"):
                    comparator = new DateStartComparator<>();
                    break;
                case ("3"):
                    comparator = new DateFinishComparator<>();
                    break;
                case ("4"):
                    comparator = new ByStatusComparator<>();
                    break;
            }
        }
        Stream stream = taskList.stream();
        @Nullable final ArrayList<Task> list = (ArrayList<Task>) stream.sorted(comparator).collect(Collectors.toCollection(ArrayList::new));
        return list;
    }
}
