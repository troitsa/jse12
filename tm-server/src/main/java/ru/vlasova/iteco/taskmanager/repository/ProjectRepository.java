package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Status;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    @Nullable
    public String getIdByIndex(@NotNull final String userId, int projectIndex) throws Exception {
        @Nullable final List<Project> projectList = findAll(userId);
        @Nullable final Project project = projectList.get(projectIndex);
        if (project == null) return null;
        @NotNull final String projectId = project.getId();
        return projectId;
    }

    @Override
    @NotNull
    public List<Project> search(@NotNull final String userId, @NotNull final String searchString) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where user_id = ? and (name = ? or description = ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, searchString);
        preparedStatement.setString(3, searchString);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    public List<Project> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where user_id = ? and id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_project where user_id = ? and id = ?");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_project where user_id = ?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Override
    @NotNull
    public Project persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "insert into taskmanager.app_project values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getUserId());
        preparedStatement.setString(3, project.getName());
        preparedStatement.setString(4, project.getDescription());
        preparedStatement.setDate(5, new java.sql.Date(project.getDateCreate().getTime()));
        preparedStatement.setDate(6, DateUtil.parseDateToSQLDate(project.getDateStart()));
        preparedStatement.setDate(7, DateUtil.parseDateToSQLDate(project.getDateFinish()));
        preparedStatement.setString(8, project.getStatus().name());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @Override
    @NotNull
    public Project merge(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "update taskmanager.app_project set user_id=?," +
                " dateCreate = ?, name = ?, description = ?, status = ?, dateStart= ?, dateFinish =? where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getUserId());
        preparedStatement.setDate(2, new java.sql.Date(project.getDateCreate().getTime()));
        preparedStatement.setString(3, project.getName());
        preparedStatement.setString(4, project.getDescription());
        preparedStatement.setString(5, String.valueOf(project.getStatus()));
        preparedStatement.setDate(6, DateUtil.parseDateToSQLDate(project.getDateStart()));
        preparedStatement.setDate(7, DateUtil.parseDateToSQLDate(project.getDateFinish()));
        preparedStatement.setString(8, project.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_project");
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement preparedStatement =
                connection.prepareStatement("delete from taskmanager.app_project where id = ?");
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public Project fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setDateCreate(row.getDate("dateCreate"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setDateStart(row.getDate("dateStart"));
        project.setDateFinish(row.getDate("dateFinish"));
        return project;
    }

}
