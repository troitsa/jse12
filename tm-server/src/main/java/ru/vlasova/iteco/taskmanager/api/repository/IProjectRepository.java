package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    @Nullable
    String getIdByIndex(@NotNull final String userId, int projectIndex) throws Exception;

    @NotNull
    List<Project> search(@NotNull final String userId, @NotNull final String searchString) throws Exception;

    @NotNull
    List<Project> findAll(@NotNull final String userId) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String id) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOne(@NotNull final String id) throws Exception;

    @Nullable
    Project persist(@NotNull final Project obj) throws DuplicateException, SQLException;

    @NotNull Project merge(@NotNull final Project obj) throws SQLException;

    void remove(@NotNull final String id) throws SQLException;

    void removeAll() throws SQLException;

    Project fetch(@Nullable final ResultSet row) throws Exception;

}
